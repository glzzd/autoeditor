<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    qm-autoeditor
 * @subpackage qm-autoeditor/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    qm_autoeditor
 * @subpackage qm_autoeditor/includes
 * @author     Ilya Glazdovsky <glsd86@gmail.com>
 */
class QM_Autoeditor_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */

	public static function activate() {
		require_once( plugin_dir_path( dirname( __FILE__ ) ) . 'includes/settings.php' );

		$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		/* SQL запрос для создания таблицы марок */
		$sql_brands = "CREATE TABLE ".GL_BRANDS_TABLE." (
		    id int(11) unsigned NOT NULL auto_increment,
		    name_eng varchar(255) NOT NULL default '',
		    name_ru varchar(255) default '',
		    image varchar(255) default '',
		    description_short varchar(1024) default '',
		    description_full text default '',
		    link_ru varchar(255) default '',
		    link_int varchar(255) default '',
		    PRIMARY KEY  (id)
		) {$charset_collate};";



		/* SQL запрос для создания таблицы моделей */
		$sql_models = "CREATE TABLE ".GL_MODELS_TABLE."(
		    id int(11) unsigned NOT NULL auto_increment,
		    brand_id int(11) unsigned NOT NULL,
		    name_eng varchar(255) NOT NULL default '',
		    name_ru varchar(255) default '',
		    image varchar(255) default '',
		    description_short varchar(1024) default '',
		    description_full text default '',
		    start_year year(4),
		    end_year year(4),
		    FOREIGN KEY (brand_id) REFERENCES ".GL_BRANDS_TABLE."(id),
		    PRIMARY KEY  (id)
		) {$charset_collate};";

		/* Выполнение sql запросов */
		dbDelta( $sql_brands );
		dbDelta( $sql_models );

	}//окончание метода activate()
}
<?php

    $action_path = admin_url("options-general.php?page=".$_GET["page"]);
    $brand_id = $_GET['id'];
    $option = $_GET['option'];

?>
<div class="wrap">
    <?php
        if(!empty($option)){
            switch ($option){
                case 'saved' :
                    echo "<div class='notice notice-success'>Сохранено</div>";
                break;
                case 'error' :
	                echo "<div class=' notice notice-error'>Поле \"Название (на английском)\" не может быть пустым.</div>";
                break;
	            case 'exceeded' :
		            echo "<div class=' notice notice-error'>Превышена максимальная длина строки!</div>";
                break;
            }
        }
    ?>

    <h2>Редактирование марки</h2>
    <p><a class="button-secondary" href="<?php echo $action_path; ?>">К списку марок</a></p>

    <form method="post" name="cleanup_options" action="<?php $_SERVER['PHP_SELF']; ?>">
	        <?php wp_nonce_field('ouRlRM8vq4nRorYZePww'); ?>
            <input type="hidden" name="brand_id" value="<?php echo $brand_id ?>">
            <fieldset>
                <label for="name_eng">Название (на английском)</label>
                <input type="text" class="large-text" id="name_eng" name="name_eng" value="<?php echo $result[0]->name_eng; ?>" maxlength="255">
            </fieldset>
            <fieldset>
                <label for="name_ru">Название (на русском)</label>
                <input type="text" class="large-text" id="name_ru" name="name_ru" value="<?php echo $result[0]->name_ru; ?>" maxlength="255">
            </fieldset>

            <fieldset>
                <label>Логотип
                    <input type="hidden" id="login_logo_id" name="login_logo_id" value="<?php echo intval($result[0]->image); ?>" />
                    <input id="upload_login_logo_button" type="button" class="button" value="Выбрать файл" />
                </label>
            </fieldset>
            <?php

                $login_logo_id = intval($result[0]->image);
                $login_logo = wp_get_attachment_image_src( $login_logo_id, 'thumbnail' );
                $login_logo_url = $login_logo[0];
            ?>
            <div id="upload_logo_preview" class="wp_cbf-upload-preview <?php if(empty($login_logo_id)) echo 'hidden'?>">
                <img src="<?php echo $login_logo_url; ?>" />
                <button id="wp_cbf-delete_logo_button" class="wp_cbf-delete-image">X</button>
            </div>
            <fieldset>
                <label>Краткое описание (до 1024 символов)
                    <textarea class="large-text tarea" name="description_short" id="" cols="30" rows="5" maxlength="1020"><?php echo $result[0]->description_short; ?></textarea>
                </label>
            </fieldset>
            <fieldset>
                <label for="descriptionfull">Полное описание</label>
	            <?php
	            wp_editor($result[0]->description_full, 'descriptionfull', array(
		            'wpautop'       => 1,
		            'media_buttons' => 0,
		            'textarea_name' => 'description_full',
		            'textarea_rows' => 10,
		            'tabindex'      => null,
		            'editor_css'    => '',
		            'editor_class'  => '',
		            'teeny'         => 0,
		            'dfw'           => 0,
		            'tinymce'       => 1,
		            'quicktags'     => 1,
		            'drag_drop_upload' => false
	            ) );
	            ?>
            </fieldset>
            <fieldset>
                <label for="link_int">Ссылка на международный сайт</label>
                <input type="text" class="large-text" id="link_int" name="link_int" value="<?php echo $result[0]->link_int; ?>" maxlength="255">
            </fieldset>
            <fieldset>
                <label for="link_ru">Ссылка на российский сайт</label>
                <input type="text" class="large-text" id="link_ru" name="link_ru" value="<?php echo $result[0]->link_ru; ?>" maxlength="255">
            </fieldset>

		<?php submit_button('Сохранить', 'primary','submit', TRUE); ?>

    </form>

</div>
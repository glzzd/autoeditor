<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to control the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    QM_Autoeditor
 * @subpackage QM_Autoeditor/admin/partials
 */
require_once( plugin_dir_path( dirname(dirname( __FILE__ ) )) . 'includes/settings.php' );

if(!class_exists('WP_List_Table')){
	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-list-table.php';
}
if(!class_exists('gl_List_Table')){
	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gl-list-table.php';
}


$qm_plugin_name = new QM_autoeditor(); $plugin_name =  $qm_plugin_name->get_plugin_name();

if($_SERVER['REQUEST_METHOD']==='GET'){

    $action = $_GET['action'];
    $id = $_GET['id'];
    if(!empty($action) and !empty($id) and hash_equals('edit',$action)){
	    $result = $wpdb->get_results("SELECT id,name_eng,name_ru,image,description_short,description_full,link_int,link_ru FROM ".GL_BRANDS_TABLE." WHERE id=$id LIMIT 1");
        require_once WP_PLUGIN_DIR.'/'.$plugin_name.'/admin/partials/qm-autoeditor-edit.php';

    }
    elseif(!empty($action) and !empty($id) and hash_equals('delete',$action)){
	    gl_List_Table::deleteBrands($id);
    }
    elseif(!empty($action) and hash_equals('add',$action)){
	    require_once WP_PLUGIN_DIR.'/'.$plugin_name.'/admin/partials/qm-autoeditor-add.php';
    }
    else{
	    require_once WP_PLUGIN_DIR.'/'.$plugin_name.'/admin/partials/qm-autoeditor-table.php';
    }
}

if($_SERVER['REQUEST_METHOD']=='POST'){
	if(! empty( $_POST['_wpnonce'] )){

		$nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );

		if ( ! wp_verify_nonce( $nonce, 'ouRlRM8vq4nRorYZePww' ) ){
			wp_die( 'Nope! Security check failed!' );
		}

	}

	$action = $_POST['action'];
	$name_eng = $_POST['name_eng'];
	$name_ru = $_POST['name_ru'];
	$image = strval($_POST['login_logo_id']);
	$description_short = $_POST['description_short'];
	$description_full = $_POST['description_full'];
	$link_int = $_POST['link_int'];
	$link_ru = $_POST['link_ru'];
	$brand_id = $_POST['brand_id'];

	if(hash_equals('add',$action)){
		if(empty($name_eng)){
			$redirect_path = $_SERVER['REQUEST_URI'].'&option=error';
			echo '<script>window.location = "'.$redirect_path.'";</script>';
		}
		elseif (strlen($name_eng)>255 or strlen($name_ru)>255 or strlen($description_short)>1024 or strlen($description_full)>63491 or strlen($link_ru)>255 or strlen($link_int)>255){
			echo strlen($description_short);
			//$redirect_path = $_SERVER['REQUEST_URI'].'&option=exceeded';
			//echo '<script>window.location = "'.$redirect_path.'";</script>';
		}
		else{
			$wpdb->insert(GL_BRANDS_TABLE,
				array(
					'name_eng'=>$name_eng,
					'name_ru'=>$name_ru,
					'image'=>$image,
					'description_short'=>strip_tags($description_short),
					'description_full'=>$description_full,
					'link_int'=>$link_int,
					'link_ru'=>$link_ru,
		)
			);
			$redirect_path = admin_url("options-general.php?page=".$_GET["page"]).'&option=saved';
			echo '<script>window.location = "'.$redirect_path.'";</script>';
		}
	}
	else{
		if(empty($name_eng)){
			$redirect_path = $_SERVER['REQUEST_URI'].'&option=error';
			echo '<script>window.location = "'.$redirect_path.'";</script>';
		}
		elseif (strlen($name_eng)>255 or strlen($name_ru)>255 or strlen($description_short)>1024 or strlen($description_full)>63491 or strlen($link_ru)>255 or strlen($link_int)>255){
			$redirect_path = $_SERVER['REQUEST_URI'].'&option=exceeded';
			echo '<script>window.location = "'.$redirect_path.'";</script>';
		}
		else{
			$wpdb->update(GL_BRANDS_TABLE,
				array(
					'name_eng'=>$name_eng,
					'name_ru'=>$name_ru,
					'image'=>$image,
					'description_short'=>strip_tags($description_short),
					'description_full'=>$description_full,
					'link_int'=>$link_int,
					'link_ru'=>$link_ru,
				),array('id'=>$brand_id)
			);
			$redirect_path = $_SERVER['REQUEST_URI'].'&option=saved';
			echo '<script>window.location = "'.$redirect_path.'";</script>';

		}
	}

}

?>
<?php
	//Create an instance of our package class...
	$testListTable = new gl_List_Table();
	//Fetch, prepare, sort, and filter our data...
	$testListTable->prepare_items();

    $option = $_GET['option'];
?>

<div class="wrap">
    <?php
    if(!empty($option)){
	    switch ($option){
		    case 'saved' :
			    echo "<div class='notice notice-success'>Запись добавлена!</div>";
			    break;
		    case 'error' :
			    echo "<div class=' notice notice-error'>Поле \"Название (на английском)\" не может быть пустым.</div>";
			    break;
            case 'deleted' :
	            echo "<div class=' notice notice-info'>Запись удалена.</div>";
	            break;
	    }
    }
    ?>
	<h2><?php echo esc_html(get_admin_page_title()); ?></h2>
    <h3>Список марок</h3>

    <p><a class="button-secondary" href="<?php echo sprintf('?page=%s&action=%s',$_REQUEST['page'],'add'); ?>">Добавить марку</a></p>

	<div id="icon-users" class="icon32"><br/></div>
	<!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
	<form id="movies-filter" method="get">
		<!-- For plugins, we also need to ensure that the form posts back to our current page -->
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
		<!-- Now we can render the completed list table -->
		<?php $testListTable->display() ?>
	</form>
</div>